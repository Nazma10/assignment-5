#include<stdio.h>

int NoOfAttendees(int);
int Revenue (int);
int Cost (int);
int Profit (int);

//a=price

int	NoOfAttendees (int a){
	return 120-(a-15)/5*20;
}

int Revenue (int a){
	return NoOfAttendees(a)*a;
}

int Cost(int a){
	return NoOfAttendees(a)*3+500;
}

int Profit(int a){
	return Revenue(a)-Cost(a);
}

int main(){
	int a;
	printf("\nExpected Profit for Ticket Prices: \n\n");
	for(a=5;a<50;a+=5)
	{
		printf("Ticket Price = Rs.%d \t\t Profit = Rs.%d\n",a,Profit(a));
		printf("-----------------------------------------------------\n\n");
    }
		return 0;
}
